//
//  ViewController.m
//  sginup
//
//  Created by Cli16 on 9/17/15.
//  Copyright (c) 2015 Cli16. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    UILabel *label;
    UILabel *label1;
    UILabel *label2;
    UILabel *label3;
    UILabel *label4;
    UILabel *label5;
    UILabel *label6;
    UITextField *textfield1;
    UITextField *textfield2;
    UITextField *textfield3;
    UITextField *textfield4;
    UITextField *textfield5;
    UITextField *textfield6;
    UIButton *button;
    UIImageView *image;

}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    image = [UIImageView new];
    image.frame = CGRectMake(0, 0, 400, 700);
    image.image = [UIImage imageNamed:@"red-wallpaper-8.jpg"];
    [self.view addSubview:image];
    
    
    
    label = [UILabel new];
    label.frame = CGRectMake(140, 10, 200, 150);
    label.text = @"SIGN UP";
    label.font = [UIFont fontWithName:@"Verdana" size:30];
    label.textColor=[UIColor whiteColor];
    //  label.
    [self.view addSubview:label];
    
    label1 = [UILabel new];
    label1.frame = CGRectMake(10, 150, 150, 35);
    label1.text = @"FirstName";
    label1.textColor=[UIColor whiteColor];
    [self.view addSubview:label1];
    
    label2 = [UILabel new];
    label2.frame = CGRectMake(10, 190, 150, 35);
    label2.text = @"LastName";
    label2.textColor=[UIColor whiteColor];
    [self.view addSubview:label2];
    
    label3 = [UILabel new];
    label3.frame = CGRectMake(10, 230, 150, 35);
    label3.text = @"Phone- No";
    label3.textColor=[UIColor whiteColor];
    [self.view addSubview:label3];
    
    label4 = [UILabel new];
    label4.frame = CGRectMake(10, 270, 150, 35);
    label4.text = @"Email";
    label4.textColor=[UIColor whiteColor];
    [self.view addSubview:label4];
    
    label5 = [UILabel new];
    label5.frame = CGRectMake(10, 310, 150, 35);
    label5.text = @"Password";
    label5.textColor=[UIColor whiteColor];
    [self.view addSubview:label5];
    
    label6 = [UILabel new];
    label6.frame = CGRectMake(10, 350, 150, 35);
    label6.text = @"Confpass";
    label6.textColor=[UIColor whiteColor];
    [self.view addSubview:label6];
    
    textfield1 = [UITextField new];
    textfield1.frame = CGRectMake(170,150, 150, 30);
    textfield1.placeholder = @"First-Name";
    textfield1.textColor=[UIColor blackColor];
    textfield1.backgroundColor =[UIColor whiteColor];
    textfield1.layer.cornerRadius = 8.0f;
    // textfield1.backgroundColor =[UIColor whiteColor];
    [self.view addSubview:textfield1];
    
    textfield2 = [UITextField new];
    textfield2.frame = CGRectMake(170,190, 150, 30);
    textfield2.placeholder = @"Lirst-Name";
    textfield2.textColor=[UIColor redColor];
    textfield2.backgroundColor =[UIColor whiteColor];
    textfield2.layer.cornerRadius = 8.0f;
    // textfield2.backgroundColor =[UIColor whiteColor];
    [self.view addSubview:textfield2];
    
    textfield3 = [UITextField new];
    textfield3.frame = CGRectMake(170,230, 150, 30);
    textfield3.placeholder = @"Phone-No";
    textfield3.textColor=[UIColor redColor];
    textfield3.backgroundColor =[UIColor whiteColor];
    textfield3.layer.cornerRadius = 8.0f;
    // textfield3.backgroundColor =[UIColor whiteColor];
    [self.view addSubview:textfield3];
    
    
    textfield4 = [UITextField new];
    textfield4.frame = CGRectMake(170,270, 150, 30);
    textfield4.placeholder = @"Email";
    textfield4.textColor=[UIColor redColor];
    textfield4.backgroundColor =[UIColor whiteColor];
    textfield4.layer.cornerRadius = 8.0f;
    // textfield4.backgroundColor =[UIColor whiteColor];
    [self.view addSubview:textfield4];
    
    textfield5 = [UITextField new];
    textfield5.frame = CGRectMake(170,310, 150, 30);
    textfield5.placeholder = @"Password";
    textfield5.textColor=[UIColor redColor];
    textfield5.backgroundColor =[UIColor whiteColor];
    textfield5.layer.cornerRadius = 8.0f;
    textfield5.secureTextEntry = YES;
    // textfield5.backgroundColor =[UIColor whiteColor];
    [self.view addSubview:textfield5];
    
    
    textfield6 = [UITextField new];
    textfield6.frame = CGRectMake(170,350, 150, 30);
    textfield6.placeholder = @"Confpass";
    textfield6.textColor=[UIColor redColor];
    textfield6.backgroundColor =[UIColor whiteColor];
    textfield6.layer.cornerRadius = 8.0f;
    textfield6.secureTextEntry = YES;
    [self.view addSubview:textfield6];
    
    
    button = [UIButton new];
    button.frame = CGRectMake(130, 410, 150, 50);
    button.layer.cornerRadius = 8.0f;
    [button addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"submit-button-blue-hi (1).png"] forState:UIControlStateNormal];
    [self.view addSubview:button];

    // Do any additional setup after loading the view, typically from a nib.
}

-(void) pressed: (UIButton *) sender{
    
    if ([self FirstName: textfield1.text] == YES){
        
        
        [self showalert1];
        
    }
    else if ([self LastName: textfield2.text] == YES){
        
        
        
        [self showalert2];
        
    }
    
    
    else if ([textfield1.text length] == 0) {
        
        [self showalert3];
        
    }
    else if([textfield2.text length] == 0){
        [self showalert3];
    }
    else if([textfield3.text length] == 0){
        [self showalert3];
    }
    else if([textfield4.text length] == 0){
        [self showalert3];
    }
    else if([textfield5.text length] == 0){
        [self showalert3];
    }
    else if([textfield6.text length] == 0){
        [self showalert3];
    }
    
    else if ([self Phoneno: textfield3.text] == YES){
        
        [self showalert4];
        
    }
    else if ([self email: textfield4.text] == NO){
        
        [self showalert7];
        
    }

    else if ([textfield5.text isEqualToString:textfield6.text]) {
        
      //  return NO;
        
    }
             else
             {
                 [self showalert8];
             }
    
   


    
    


}

-(BOOL) FirstName:(NSString*)text{
    
    NSCharacterSet *set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ]invertedSet];
    
    if ([text rangeOfCharacterFromSet:set].location == NSNotFound){
        
        
        
        NSLog(@"SPECIAL CHARETER" );
        
        return NO;
        
    }
    
    
    
    else
        
    {
        
        NSLog(@"NO CHARECTER FOUND");
        
        return YES;
        
        
        
    }
    
    
    
    return NO;
    
    
    
}

-(BOOL) LastName:(NSString*)text{
    
    NSCharacterSet *set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ]invertedSet];
    
    if ([text rangeOfCharacterFromSet:set].location == NSNotFound){
        
        
        
        NSLog(@" SPECIAL CHARETER" );
        
        return NO;
        
    }
    
    
    
    else
        
    {
        
        NSLog(@"NO CHARECTER FOUND");
        
        return YES;
        
        
        
    }
    
    
    
    return NO;
    
    
    
}

/*NSString *password = textfield7.text;
NSString *confirmPassword = textfield8.text;

if([password isEqualToString: confirmPassword]) {
    
} else {
   
}*/

-(BOOL) Phoneno:(NSString*)text{
    
    NSCharacterSet *set = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890" ]invertedSet];
    
    if ([text rangeOfCharacterFromSet:set].location == NSNotFound){
        
        
        
        NSLog(@" no SPECIAL CHARETER" );
        
        return NO;
        
    }
    
    
    
    else
        
    {
        
        NSLog(@" CHARECTER FOUND");
        
        return YES;
        
        
        
    }
    
    
    
    return NO;
    
    
    
}


- (BOOL)email:(NSString*)sender
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if ([emailTest evaluateWithObject:textfield4.text]==YES)
    {
        return YES;
    }
    else{
        NSLog(@"EMAIL not correct");
        return NO;
    }
    //return [emailTest evaluateWithObject:email];
}




-(void) showalert1 {
    
    
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"only character are allowd" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"continue..", nil];
    
    [alert show];
    
}



-(void)showalert2{
    
    
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"" message:@"only character are allowd" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    
    [alert1 show];
    
    
    
}



-(void)showalert3{
    
    
    
    UIAlertView *alert2 = [[UIAlertView alloc]initWithTitle:@"" message:@"All Field Mandetary" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    
    [alert2 show];
    
    
    
}



-(void)showalert4{
    
    
    
    UIAlertView *alert3 = [[UIAlertView alloc]initWithTitle:@"" message:@"Only Digits Are Allowed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    
    [alert3 show];
    
    
    
}



-(void)showalert5{
    
    
    
    UIAlertView *alert4 = [[UIAlertView alloc]initWithTitle:@"" message:@"Only Digits Are Allowed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    
    [alert4 show];
    
    
    
}



-(void)showalert6{
    
    
    
    UIAlertView *alert5 = [[UIAlertView alloc]initWithTitle:@"" message:@"Only Digits Are Allowed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    
    [alert5 show];
    
    
    
}


-(void)showalert7{
    
    
    
    UIAlertView *alert6 = [[UIAlertView alloc]initWithTitle:@"" message:@"emailnotcorrect" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    
    [alert6 show];
    
    
    
}


-(void)showalert8{
    
    
    
    UIAlertView *alert7 = [[UIAlertView alloc]initWithTitle:@"" message:@"PassnotMatch" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    
    [alert7 show];
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
